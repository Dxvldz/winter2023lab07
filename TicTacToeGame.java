import java.util.Scanner;

public class TicTacToeGame {
    public static void main(String[]args) {
        System.out.println("WELCOME PEOPLE TO TIC TAC TOE!!");
        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
        Scanner scan = new Scanner(System.in);
        int row;
        int col;
        int p1Win = 0;
        int p2Win = 0;

        while(!gameOver) {
            System.out.println(board);

            if (player == 1) {
                playerToken = Square.X;
            }
            else {
                playerToken = Square.O;
            }

            do {
                System.out.println("Please input row you want to place your token.");
                row = scan.nextInt();
                System.out.println("Please input column you want to place your token.");
                col = scan.nextInt();
            } while (!board.placeToken(row, col, playerToken));

            if (board.checkifFull()) {
                System.out.println("It's a tie!");
                gameOver = true;

                System.out.println("Would you like to play again?(yes or no)");
                String answer = scan.next();
                if (answer.equalsIgnoreCase("yes")) {
                    board = new Board();
                    gameOver = false;
                }
                else if (answer.equalsIgnoreCase("no")) {
                    System.out.println("Thank you for playing!");
                }
            }
            else if (board.checkifWinning(playerToken)) {
                System.out.println(board);
                System.out.println("Player " + player + " is the winner");
                gameOver = true;

                if (player == 1) {
                    p1Win++;
                }
                else {
                    p2Win++;
                }
                System.out.println("Player 1 has " + p1Win + " wins");
                System.out.println("Player 2 has " + p2Win + " wins");

                System.out.println("Would you like to play again? (yes or no)");
                String answer = scan.next();
                if (answer.equalsIgnoreCase("yes")) {
                    board = new Board();
                    gameOver = false;
                }
                else if (answer.equalsIgnoreCase("no")) {
                    System.out.println("Thank you for playing!");
                }
            }
            else {
                player ++;
                if (player > 2) {
                    player = 1;
                }
            }


        }
    }
}
