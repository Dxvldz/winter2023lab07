public class Board {
    private Square[][] ticTacToeBoard;

    public Board() {
        this.ticTacToeBoard = new Square[3][3];
        for (int i = 0 ; i < this.ticTacToeBoard.length ; i++) {
            for (int j = 0 ; j < this.ticTacToeBoard[i].length ; j++) {
                this.ticTacToeBoard[i][j] = Square.BLANK;
            }
        }
    }

    public String toString() {
        String str = "  0 1 2 \n";
        for (int i = 0 ; i < this.ticTacToeBoard.length ; i++) {
            str = str + i + " ";
            for (int j = 0 ; j < this.ticTacToeBoard[i].length ; j++) {
                str = str + this.ticTacToeBoard[i][j] + " ";
            }
            str = str + "\n";
        }
        return str;
    }

    public boolean placeToken (int row, int col, Square playerToken) {
        if (row > 2 || row < 0 || col > 2 || col < 0) {
            return false;
        }
        if (this.ticTacToeBoard[row][col] == Square.BLANK) {
            this.ticTacToeBoard[row][col] =  playerToken;
            return true;
        }
        else {
            return false;
        }
    }

    public boolean checkifFull () {
        for (int i = 0 ; i <  this.ticTacToeBoard.length ; i++) {
            for (int j = 0 ; j < this.ticTacToeBoard[i].length  ; j++) {
                if (this.ticTacToeBoard[i][j] == Square.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkifWinningHorizontal(Square playerToken) {
        int winnerCount = 0;
        for (int i = 0 ; i < this.ticTacToeBoard.length  ; i++) {
            for (int j = 0 ; j < this.ticTacToeBoard[i].length ; j++) {
                if (this.ticTacToeBoard[i][j] == playerToken) {
                    winnerCount++;
                    if (winnerCount == 3) {
                        return true;
                    }
                }
            }
            winnerCount = 0;
        }
        return false;
    }

    private boolean checkifWinningVertical (Square playerToken) {
        int winnerCount = 0;
        for (int i = 0 ; i < this.ticTacToeBoard.length  ; i++) {
            for (int j = 0 ; j < this.ticTacToeBoard[i].length ; j++) {
                if (this.ticTacToeBoard[j][i] == playerToken) {
                    winnerCount++;
                    if (winnerCount == 3) {
                        return true;
                    }
                }
            }
            winnerCount = 0;
        }
        return false;
    }

    private boolean checkifWinningDiagonal (Square playerToken) {
        int winnerCount = 0;
        for (int i = 0 ; i < this.ticTacToeBoard.length ; i++) {
                if (this.ticTacToeBoard[i][i] == playerToken) {
                    winnerCount++;
                    if (winnerCount == 3) {
                        return true;
                    }
                }
        }

        winnerCount = 0;
        
        for (int i = 0 ; i < this.ticTacToeBoard.length ; i++ ) {
            for (int j = 0 ; j < this.ticTacToeBoard[i].length ; j++ ) {
                if ((i+j) == (this.ticTacToeBoard.length - 1 )) {
                    if (this.ticTacToeBoard[i][j] == playerToken) {
                        winnerCount++;
                        if ( winnerCount == 3) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    

    public boolean checkifWinning(Square playerToken) {
        if (checkifWinningHorizontal(playerToken) || checkifWinningVertical(playerToken) || checkifWinningDiagonal(playerToken)) {
            return true;
        }
        return false;
    }
}
